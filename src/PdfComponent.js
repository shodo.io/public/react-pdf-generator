import React, {useRef} from 'react';
import BillComponent from './bill/BillComponent';
import html2pdf from "html2pdf.js";

export const PdfComponent = () => {
    const exportRef = useRef();
    return (
        <>
            <BillComponent ref={exportRef}/>
            <button onClick={() => html2pdf(exportRef.current, {filename: 'test'})}>
                Capture Image
            </button>
        </>
    );
}

export default PdfComponent;
