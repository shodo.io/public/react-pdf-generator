import React from 'react';
import logo from '../images/logo.png';
import {
    Bill,
    BillDate,
    BillDesignationCenteredSpan,
    BillDesignationDiv,
    BillDesignationLeftAlignSpan,
    BillDetailsBody,
    BillDetailsFirstTd,
    BillDetailsFooter,
    BillDetailsHeader,
    BillDetailsHeaderFirstTh,
    BillDetailsHeaderTh,
    BillDetailsRow,
    BillDetailsTable,
    BillDetailsTd,
    BillHeaderDiv,
    BillNumber,
    BillOffsetDiv,
    BillOrder,
    BillOrderNumber,
    BillProductsSummary,
    BillSummaryDiv,
    CanonOce,
    CenteredDiv,
    CodeUF,
    HospitalAddress,
    HospitalContact,
    HospitalReprography,
    Logo,
    Offset,
    RightAlignDiv,
    SpaceBetweenDiv
} from "./styled";

const BillHeader = () => {
    return (
        <BillHeaderDiv>
            <Logo>
                <img src={logo} alt="Logo" width={'120px'} height={'120px'}/>
            </Logo>
            <HospitalContact>
                <HospitalAddress>
                    <span>
                        CENTRE HOSPITALIER REGIONAL METZ-THIONVILLE
                    </span>
                    <span>
                        1, allée du château - 57530 Ars-Laquenexy
                    </span>
                </HospitalAddress>
                <HospitalReprography>
                    <span>
                        Service Reprographie - M. Langgartner - Tél : 03 87 56 23 94
                    </span>
                    <span>
                        27, avenue de Plantières - HIA Legouest Bâtiment 55
                    </span>
                    <span>
                        57070 METZ Cedex 3
                    </span>
                </HospitalReprography>
            </HospitalContact>
        </BillHeaderDiv>
    );
}

const BillOffset = () => {
    return (
        <BillOffsetDiv>
            <BillDate>
                Le: 03/10/2017
            </BillDate>
            <Offset>
                OFFSET
            </Offset>
            <CanonOce>
                <span>Canon</span>
                <span>Oce</span>
            </CanonOce>
        </BillOffsetDiv>
    );
}

const BillSummary = () => {
    return (
        <BillSummaryDiv>
            <BillNumber>
                        <span>
                            Facture n° :
                        </span>
                <span>
                            2017-001004
                        </span>
            </BillNumber>
            <BillOrder>
                <BillOrderNumber>
                            <span>
                                N° de commande client :
                            </span>
                    <span>
                                000005
                            </span>
                </BillOrderNumber>
                <BillProductsSummary>
                            <span>
                                Nbre de produits dans la cde :
                            </span>
                    <span>
                                1
                            </span>
                </BillProductsSummary>
            </BillOrder>
            <CodeUF>
                <span>
                    Code UF :
                </span>

                <span>
                    OFFSET
                </span>
            </CodeUF>
        </BillSummaryDiv>
    );
}
const BillDetailsCell = ({colSpan = 1, rowSpan = 1, first = false, children}) => {
    return (
        first ? <BillDetailsFirstTd colSpan={colSpan}
                                    rowSpan={rowSpan}><CenteredDiv>{children}</CenteredDiv></BillDetailsFirstTd>
            : <BillDetailsTd colSpan={colSpan} rowSpan={rowSpan}><CenteredDiv>{children}</CenteredDiv></BillDetailsTd>
    )
}

const BillDetailsCellRightAlign = ({colSpan = 1, rowSpan = 1, style, children}) => {
    return (
        <BillDetailsTd colSpan={colSpan} rowSpan={rowSpan}
                       style={style}><RightAlignDiv>{children}</RightAlignDiv></BillDetailsTd>
    )
}

const BillDetailsCellSpaceBetween = ({colSpan = 1, rowSpan = 1, style, children}) => {
    return (
        <BillDetailsTd colSpan={colSpan} rowSpan={rowSpan}
                       style={style}><SpaceBetweenDiv>{children}</SpaceBetweenDiv></BillDetailsTd>
    )
}

const BillDetailsHeaderCell = ({colSpan = 1, width, rowSpan = 1, first = false, children}) => {
    return (
        first ? <BillDetailsHeaderFirstTh colSpan={colSpan} width={width}
                                          rowSpan={rowSpan}><CenteredDiv>{children}</CenteredDiv></BillDetailsHeaderFirstTh> :
            <BillDetailsHeaderTh colSpan={colSpan} width={width}
                                 rowSpan={rowSpan}><CenteredDiv>{children}</CenteredDiv></BillDetailsHeaderTh>
    )
}

const BillDesignation = () => {
    return (
        <BillDesignationDiv>
            <BillDesignationCenteredSpan> CHRU CARNET 4 INJECTIONS</BillDesignationCenteredSpan>
            <BillDesignationLeftAlignSpan> Dont (€HT): <span> {undefined}</span></BillDesignationLeftAlignSpan>
            <BillDesignationLeftAlignSpan> Impression Couleur (Pages)</BillDesignationLeftAlignSpan>
            <BillDesignationLeftAlignSpan> Amortissement couleur (Pages)</BillDesignationLeftAlignSpan>
            <BillDesignationLeftAlignSpan> Charges couleur (Feuilles)</BillDesignationLeftAlignSpan>
            <BillDesignationLeftAlignSpan> Fourniture et prestations
                : <span>{undefined}</span></BillDesignationLeftAlignSpan>
        </BillDesignationDiv>
    )
}
const BillDetails = () => {
    return (<BillDetailsTable>
        <BillDetailsHeader>
            <BillDetailsHeaderCell width="8%" first={true}>N° de travail </BillDetailsHeaderCell>
            <BillDetailsHeaderCell width="50%">Désignation</BillDetailsHeaderCell>
            <BillDetailsHeaderCell width="10%">Recto/Verso</BillDetailsHeaderCell>
            <BillDetailsHeaderCell width="10%">Quantité</BillDetailsHeaderCell>
            <BillDetailsHeaderCell width="14%">Montant &euro;HT</BillDetailsHeaderCell>
        </BillDetailsHeader>
        <BillDetailsBody>
            <BillDetailsRow>
                <BillDetailsCell first={true}> 5 </BillDetailsCell>
                <BillDetailsCell> <BillDesignation/></BillDetailsCell>
                <BillDetailsCell> RV </BillDetailsCell>
                <BillDetailsCellRightAlign> 1</BillDetailsCellRightAlign>
                <BillDetailsCellRightAlign> 0.00</BillDetailsCellRightAlign>
            </BillDetailsRow>
            <BillDetailsRow>
                <BillDetailsCell first={true}> 5 </BillDetailsCell>
                <BillDetailsCell> <BillDesignation/></BillDetailsCell>
                <BillDetailsCell> RV </BillDetailsCell>
                <BillDetailsCellRightAlign> 1</BillDetailsCellRightAlign>
                <BillDetailsCellRightAlign> 0.00</BillDetailsCellRightAlign>
            </BillDetailsRow>
        </BillDetailsBody>
        <BillDetailsFooter>
            <BillDetailsRow>
                <BillDetailsCell first={true} colSpan={2} rowSpan={3}/>
                <BillDetailsCellSpaceBetween colSpan={3}>
                    <span>Total H.T.</span>
                    <span>€O.OO</span>
                </BillDetailsCellSpaceBetween>
            </BillDetailsRow>
            <BillDetailsRow>
                <BillDetailsCellSpaceBetween colSpan={3}>
                    <span>TVA 20 %</span>
                    <span>€-</span>
                </BillDetailsCellSpaceBetween>
            </BillDetailsRow>
            <BillDetailsRow>
                <BillDetailsCellSpaceBetween colSpan={3}
                                             style={{borderTop: '2px solid black', fontWeight: 'bold'}}>
                    <span>Total T.T.C.</span>
                    <span>€O.OO</span>
                </BillDetailsCellSpaceBetween>
            </BillDetailsRow>
        </BillDetailsFooter>
    </BillDetailsTable>);
}

const BillComponent = React.forwardRef((props, ref) => (
    <Bill ref={ref}>
        <BillHeader/>
        <BillOffset/>
        <BillSummary/>
        <BillDetails/>
    </Bill>
));

export default BillComponent;
