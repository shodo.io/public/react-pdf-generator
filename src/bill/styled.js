import styled from "styled-components";

export const Bill = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
  width: 800px;
`;

export const BillHeaderDiv = styled.div`
  display: flex;
  padding: 20px;
`;

export const Logo = styled.div`
  border-bottom: 1px solid darkgrey;
  border-left: 1px solid darkgrey;
  border-right: 2px solid darkgrey;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;
  width: 150px;
  height: 150px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 20px;
`;

export const HospitalContact = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 20px;
  justify-content: space-between;
`;

export const HospitalAddress = styled.div`
  display: flex;
  font-size: 18px;
  font-weight: bold;
  flex-direction: column;
  align-items: center;
`;

export const HospitalReprography = styled.div`
  display: flex;
  font-size: 18px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const BillOffsetDiv = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  border: 1px solid black;
  height: 130px;
`;

export const BillDate = styled.div`
  display: flex;
  padding-right: 100px;
  justify-content: flex-end;
  width: 100%;
  font-size: 12px;
`;

export const Offset = styled.div`
  display: flex;
  justify-content: flex-start;
  font-size: 12px;
`;

export const CanonOce = styled.div`
  display: flex;
  font-size: 12px;
  width:30%;
  justify-content: space-between;
`;

export const BillSummaryDiv = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  border: 1px solid black;
  background-color: darkgrey;
  margin-top: 20px;
`;

export const BillNumber = styled.div`
  display: flex;
  font-weight: bold;
  align-items: flex-start;
  width:30%;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const BillOrder = styled.div`
  font-size: 12px;
  display: flex;
  align-items: flex-start;
  margin-bottom: 10px;
  justify-content: space-between;
`;

export const BillOrderNumber = styled.span`
  display: flex;
  align-items: flex-start;
  width:40%;
  justify-content: space-between;
`;

export const BillProductsSummary = styled.span`
  display: flex;
  width:30%;
  margin-right: 100px;
  justify-content: space-between;
  font-style: italic;
`;

export const CodeUF = styled.div`
  display: flex;
  font-weight: bold;
  align-items: flex-start;
  width:20%;
  justify-content: space-between;
`;

export const BillDetailsHeader = styled.thead`
  font-weight: bold;
  border: 1px solid black;
`;

export const BillDetailsHeaderTh = styled.th`
  border-left: 1px solid black;
`;

export const BillDetailsHeaderFirstTh = styled.th`
  
`;

export const BillDetailsBody = styled.tbody`
  font-size: 12px;
  border-top: 2px solid black;
`;

export const BillDetailsFooter = styled.tfoot`
  font-size: 12px;
  border-top: 2px solid black;
`;

export const BillDetailsRow = styled.tr`
  border: 1px solid black;
`;

export const BillDetailsFirstTd = styled.td``;
export const BillDetailsTd = styled.td`
  border-left: 1px solid black;
`;

export const CenteredDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  justify-items: center;
`;

export const RightAlignDiv = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
  justify-items: flex-end;
  padding-right: 10px;
`;

export const SpaceBetweenDiv = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding-right: 10px;
  padding-left: 10px;
`;

export const BillDesignationDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
  font-size: 10px;
  font-style: italic;
  padding: 5px;
`;

export const BillDesignationLeftAlignSpan = styled.span`
    margin-top: 5px;
`;

export const BillDesignationCenteredSpan = styled.span`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  font-size: 12px;
  font-style: normal;
  margin-top: 5px;
`;

export const BillDetailsTable = styled.table`
  margin-top: 20px;
`;
