import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import PdfComponent from './PdfComponent';

function App() {
    return (
        <div className="App">
            <PdfComponent />
        </div>
    );
}

export default App;
